from django.conf.urls import url

from . import views

app_name = 'member_check'

urlpatterns = [
    url(r'^$', views.dashboard, name='dashboard')
]
