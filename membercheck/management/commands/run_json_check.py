from django.core.management.base import BaseCommand
from allianceauth.authentication.models import User, UserProfile
from allianceauth.eveonline.models import EveCharacter
import esi
from memberaudit.models import Character
from esi.clients import EsiClientProvider

import json
import os


class Command(BaseCommand):
    help = "Runs the member check against SeAT data."

    def add_arguments(self, parser):
        parser.add_argument(
            '--file',
            type=str,
            default="members.json",
            help="The path to the members.json file."
        )

    def handle(self, *args, **options):
        esi = EsiClientProvider(app_info_text="AA Seat Member Checker").client

        if not os.path.exists(options['file']):
            self.stdout.write(self.style.WARNING("members.json does not exist at the provided location."))
            return
        else:
            missing = dict()
            with open(options['file'], 'r') as file:
                members = json.loads(file.read())
            
            for k, v in members.items():
                mains = list()
                not_registered = list()
                for char in v:
                    # Get the evecharacter
                    evechar = EveCharacter.objects.filter(character_id=char)
                    if len(evechar) == 0:
                        charesi = esi.Character.get_characters_character_id(character_id=char).results()
                        charcorp = esi.Corporation.get_corporations_corporation_id(corporation_id=charesi['corporation_id']).results()
                        not_registered.append(f"{charesi['name']} // {charcorp['name']}")
                    else:
                        if UserProfile.objects.filter(main_character=evechar[0]).exists():
                            mains.append(evechar[0].character_name)
                
                if len(not_registered) >= 1:
                    seat_main = esi.Character.get_characters_character_id(character_id=int(k)).results()
                    multi_account = False
                    if len(mains) > 1:
                        multi_account = True
                    
                    value = {
                        'multi_account': multi_account,
                        'mains': mains if len(mains) > 0 else False,
                        'missing': not_registered
                    }
                    missing[seat_main['name']] = value

            with open("/opt/allianceauth/report.json", 'w') as file:
                file.write(json.dumps(missing))
