from allianceauth import hooks
from allianceauth.services.hooks import MenuItemHook, UrlHook

from . import urls


@hooks.register('url_hook')
def register_url():
    return UrlHook(urls, 'member_check', '^membcheck/')
