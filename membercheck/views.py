from django.shortcuts import render
from memberaudit.models import Character, CharacterAsset, Location
from eveuniverse.models import EveType
from django.contrib.auth.decorators import login_required, permission_required


# Create your views here.
@login_required
@permission_required('memberaudit.view_everything')
def dashboard(request):
    ctx = dict()
    location = Location.objects.get(id=1034877491366)
    ships = EveType.objects.filter(eve_group_id__in=(30, 547, 659, 485, 883, 902))

    assets = CharacterAsset.objects.filter(location=location, eve_type__in=ships)

    ctx['assets'] = assets

    return render(request, 'membercheck/dash.html', context=ctx)
